using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrigger : MonoBehaviour
{
    [Header("Increase Volume")]
    [SerializeField]
    private string soundToPlay;
    [SerializeField]
    private float increaseTargetVolume;
    [SerializeField]
    private float increaseDuration;

    [Header("Decrease Volume")]
    [SerializeField]
    private string soundToStop;
    [SerializeField]
    private float decreaseTargetVolume;
    [SerializeField]
    private float decreaseDuration;

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            if(!SFXMgr.MyInstance.isPlaying(soundToPlay))
            {        
                SFXMgr.MyInstance.PlaySound(soundToPlay);      
            }
            if(SFXMgr.MyInstance.isPlaying(soundToPlay))
            {
                SFXMgr.MyInstance.AdjustVolume(soundToPlay, increaseTargetVolume, increaseDuration);
            }
            if(soundToStop != null)
            {       
                SFXMgr.MyInstance.AdjustVolume(soundToStop, decreaseTargetVolume, decreaseDuration); 
            }
        }
    }
}
