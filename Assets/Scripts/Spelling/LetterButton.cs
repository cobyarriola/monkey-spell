using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LetterButton : MonoBehaviour, IPointerDownHandler
{
    private Image myIcon;

    [SerializeField]
    private Sprite disabled;

    [SerializeField]
    private Sprite activated;

    private Text myText;

    private int id;
    private void Awake()
    {
        myIcon = GetComponent<Image>();
        myText = GetComponentInChildren<Text>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GetComponent<Button>().interactable != false)
        {
            if (SpellZone.Instance.AddLetter(GetComponentInChildren<Text>().text, id))
            {
                SFXMgr.MyInstance.PlaySound("Select");
                Disable();
            }
        }
    }

    public void AddLetter(string newLetter)
    {
        myText.text = newLetter;
    }

    public bool ReturnLetter(int returnedID)
    {
        if (returnedID == id)
        {
            SFXMgr.MyInstance.PlaySound("Erase");
            Activate();
            return true;
        }

        return false;
    }

    public void Disable()
    {
        myIcon.sprite = disabled;
        GetComponent<Button>().interactable = false;
    }

    public void Activate()
    {
        myIcon.sprite = activated;
        GetComponent<Button>().interactable = true;
    }

    public void ChangeID(int newID)
    {
        id = newID;
    }
}