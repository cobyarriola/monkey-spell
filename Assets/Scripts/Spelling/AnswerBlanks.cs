using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AnswerBlanks : MonoBehaviour, IPointerDownHandler
{
    private Text myText;

    public Text MyText
    {
        get
        {
            return myText;
        }
    }

    private int id;

    private bool isConstant;

    private Image myImage;

    [SerializeField]
    private Sprite activated, deactivated;
    private void Awake()
    {
        myImage = GetComponent<Image>();
        myText = GetComponentInChildren<Text>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (myText.text != string.Empty && !isConstant)
        {
            myText.text = null;
            SpellZone.Instance.ReturnLetter(id);
            myImage.sprite = deactivated;
        }
    }
    public void AddLetter(string newLetter)
    {
        myText.text = newLetter;
        myImage.sprite = activated;
    }
    public void Reset()
    {
        if (myText.text != string.Empty && !isConstant)
        {
            myText.text = string.Empty;
            myImage.sprite = deactivated;
        }
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
        id = -1;
        myText.color = Color.white;
        myImage.enabled = true;
        isConstant = false;
        myText.text = null;
        myImage.sprite = deactivated;
    }

    public void ChangeID(int newID)
    {
        id = newID;
    }

    public void MakeConstant(string consText)
    {
        gameObject.SetActive(true);
        if (myText.text != string.Empty && !isConstant)
        {
            myText.text = null;
            SpellZone.Instance.ReturnLetter(id);
        }
        myText.text = consText;
        myText.color = Color.black;
        myImage.enabled = false;
        isConstant = true;
    }

    public bool IsConstant()
    {
        return isConstant;
    }
}
