using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellZone : MonoBehaviour
{
    private static SpellZone instance;
    public static SpellZone Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SpellZone>();
            }

            return instance;
        }
    }

    string[] Alphabet = new string[26] 
    { "A", "A", "B", "C", "D", "E", "E", "F", "G", "H", "I", "J",
        "K", "L", "M", "N", "O", "O", "P", "Q", "R", 
        "S", "T", "U", "V", "W"};

    //{ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
    //    "K", "L", "M", "N", "O", "P", "Q", "R", 
    //    "S", "T", "U", "V", "W", "X", "Y", "Z" };

    public Word currentWord;

    private string blankedWordString;

    private List<int> charRemaining = new List<int>();
    private List<int> charDeleted = new List<int>();

    private AudioSource currentAudio;

    [SerializeField]
    private Image currentImage;

    [SerializeField]
    private char[] letters;

    [SerializeField]
    private LetterButton[] letterButtons;

    [SerializeField]
    private AnswerBlanks[] answerBlanks;

    [SerializeField]
    private string formedWord;

    private CanvasGroup canvasGroup;

    private float myTimer;

    private bool currentTickStatus = false;

    [SerializeField]
    private Text timerText;

    public GameObject timerIcon;

    private int pointsToEarn;

    [SerializeField]
    private CameraManager myCamera;

    [SerializeField]
    private GameObject checkMark;

    [SerializeField]
    private GameObject wrongMark;

    private Coroutine hourglass;

    [SerializeField]
    private Text multiplierText;

    private int consecutive;

    private int comboMultiplier;

    [SerializeField]
    private Animator addEffect;

    [SerializeField]
    private Text sentenceText;

    [SerializeField]
    private Text endShowWord;

    [SerializeField]
    private Text correctWrongText;

    [SerializeField]
    public Health health;

    private float easyTime, moderateTime, hardTime;

    [SerializeField]
    private GameObject blur;

    [SerializeField]
    private float showingWordSpeed;

    [SerializeField]
    private GameObject secondRowAnswer;

    [SerializeField]
    private int hintUses;

    [SerializeField]
    private Text hintDisplayUses;

    private bool sevenEightWord;

    [SerializeField]
    private Text endSentence;

    [SerializeField]
    private GameObject multiplierEffect;

    private bool isCarrot;

    public bool tutorialFirstWord;

    float time;

    public delegate void onMultiplier();
    public onMultiplier OnMultiplierUp;

    public delegate void onWrong();
    public onWrong OnWrong;

    public delegate void onCorrect();
    public onCorrect OnCorrect;

    public void Start()
    {
        //multiplierEffect = Resources.Load<GameObject>("MultiplierEffect");
        consecutive = 0;
        comboMultiplier = 1;
        canvasGroup = GetComponent<CanvasGroup>();
        multiplierText.text = comboMultiplier.ToString() + "x";
        hintDisplayUses.text = hintUses.ToString();
        tutorialFirstWord = false;
        for (int i = 0; i < letterButtons.Length; i++)
        {
            letterButtons[i].ChangeID(i);
        }
    }

    public void SetTimer(float newTime, Difficulty difficulty)
    {
        if (difficulty == Difficulty.Easy)
        {
            easyTime = newTime;
        }
        if (difficulty == Difficulty.Moderate)
        {
            moderateTime = newTime;
        }
        if (difficulty == Difficulty.Hard)
        {
            hardTime = newTime;
        }
    }

    private float GetTime(Difficulty difficulty, bool isTutorialFirstWord)
    {
        if (isTutorialFirstWord == true)
        {
            time = 99;
            tutorialFirstWord = false;
            return time;
        }

        if (consecutive == 0 || consecutive == 1)
        {
            time = 15; // Change if required
            return time;
        }

        if (consecutive % 3 == 0)
        {
            time = (time - (consecutive / 3));
        }

        if (time < 9)
        {
            time = 8.7f;
        }
        return time;
    }

    private int CalculatePoints(Word newWord)
    {
        if (Difficulty.Easy == newWord.MyDifficulty)
        {
            return 500 * comboMultiplier;
        }
        if (Difficulty.Moderate == newWord.MyDifficulty)
        {
            return 1500 * comboMultiplier;
        }
        if (Difficulty.Hard == newWord.MyDifficulty)
        {
            return 3000 * comboMultiplier;
        }
        return 1;
    }

    public void ChangeWord(Word newWord)
    {
        myTimer = GetTime(newWord.MyDifficulty, tutorialFirstWord);
        hourglass = StartCoroutine(Hourglass());
        currentWord = newWord;
        currentImage.sprite = currentWord.MyImage;
        pointsToEarn = CalculatePoints(newWord);
        currentAudio = Instantiate(newWord.MyAudio);
        if (currentWord.MyWord.Length > 7 && currentWord.MyWord.Length < 10)
        {
            sevenEightWord = true;
        }

        int numOfBlanks = 0;
        //---------------------------------------------------RANDOMIZING BLANKS IN WORD (START)
        blankedWordString = currentWord.MyWord;

        for (int i = 0; i < currentWord.MyWord.Length; i++)
        {
            charRemaining.Add(i);
        }

        if (currentWord.MyDifficulty == Difficulty.Easy)
        {
            numOfBlanks = Random.Range(0, 1);
        }
        if (currentWord.MyDifficulty == Difficulty.Moderate)
        {
            if (currentWord.MyWord.Length > 12)
            {
                numOfBlanks = Random.Range(4, 5);
            }
            else
            {
                numOfBlanks = Random.Range(2, 3);
            }
        }
        if (currentWord.MyDifficulty == Difficulty.Hard)
        {
            if (currentWord.MyWord.Length > 12)
            {
                numOfBlanks = Random.Range(4, 5);
            }
            else
            {
                numOfBlanks = Random.Range(2, 3);
            }
        }

        if (currentWord.MyWord.Length > 5)
        {
            secondRowAnswer.SetActive(true);
        }
        for (int i = 0; i < numOfBlanks; i++)
        {
            AddBlanks();
        }
        AddSentence();

        for (int i = 0; i < currentWord.MyWord.Length; i++)
        {
            if (sevenEightWord && i > 4)
            {
                answerBlanks[i].transform.SetParent(transform);
                answerBlanks[i].transform.SetParent(secondRowAnswer.transform);
            }

            answerBlanks[i].Activate();
        }
        sentenceText.text = currentWord.MySentence;

        //----------------------------------------------------------
        string tmpLetters = blankedWordString;
        for (int i = blankedWordString.Length; i < letters.Length; i++)
        {
            tmpLetters += Alphabet[Random.Range(0, Alphabet.Length)];
        }

        letters = tmpLetters.ToCharArray(); 

        for (int i = letters.Length - 1; i > 0; i--)
        {
            int rnd = Random.Range(0, i);
            char temp = letters[i];
            letters[i] = letters[rnd];
            letters[rnd] = temp;
        }

        for (int i = 0; i < letters.Length; i++)
        {
            letterButtons[i].AddLetter(letters[i].ToString());
        }
    }

    public void AddBlanks()
    {
        int rand = Random.Range(0, charRemaining.Count);

        int number = charRemaining[rand];
        for (int i = 0; i < charDeleted.Count; i++)
        {
            if (charDeleted[i] < charRemaining[rand])
            {
                number--;
            }
        }

        charDeleted.Add((charRemaining[rand]));

        blankedWordString = blankedWordString.Remove(number, 1);
        answerBlanks[charRemaining[rand]].MakeConstant(currentWord.MyWord[charRemaining[rand]].ToString());

        charRemaining.RemoveAt(rand);
    }

    private void AddSentence()
    {
        //---------------------------------------------------ADDING SENTENCE WITH BLANKS(START)
        string underScoredWord = "";
        bool isSame = false;
        for (int i = 0; i < currentWord.MyWord.Length; i++)
        {
            isSame = false;
            for (int j = 0; j < charDeleted.Count; j++)
            {
                if (i == charDeleted[j])
                {
                    underScoredWord += answerBlanks[i].MyText.text;
                    isSame = true;
                    break;
                }
            }

            if (isSame)
            {
                continue;
            }

            underScoredWord += "_";
        }
        currentWord.SetBlankedWord(underScoredWord);
    }

    public bool AddLetter(string newLetter, int newID)
    {
        for (int i = 0; i < answerBlanks.Length; i++)
        {
            if (blankedWordString.Length <= formedWord.Length)
            {
                break;
            }
            if (answerBlanks[i].IsConstant())
            {
                continue;
            }

            if (answerBlanks[i].MyText.text == string.Empty)
            {
                answerBlanks[i].AddLetter(newLetter);
                answerBlanks[i].ChangeID(newID);
                formedWord += newLetter;
                Reread();
                CheckWord();
                return true;
            }
        }
        return false;
    }

    public void ReturnLetter(int id)
    {
        for (int i = 0; i < letterButtons.Length; i++)
        {
            if (letterButtons[i].ReturnLetter(id))
            {
                Reread();
                return;
            }
        }
    }
    
    public void CheckWord()
    {
        if (blankedWordString.ToUpper() == formedWord.ToUpper())
        {
            StartCoroutine(Correct());
            if(GameManager.Instance.isTutorial)
            {
                TutorialManager.Instance.BananaClickedPopup.SetActive(false);                  
            }
        }
        else if (blankedWordString.Length == formedWord.Length)
        {
            GetComponent<Animator>().SetTrigger("Play");
        }
    }

    public void ResetFormed()
    {
        SFXMgr.MyInstance.PlaySound("Reset");
        formedWord = string.Empty;
        for (int i = 0; i < answerBlanks.Length; i++)
        {
            if (answerBlanks[i].MyText == null)
            {
                break;
            }

            answerBlanks[i].Reset();
        }

        for (int i = 0; i < letterButtons.Length; i++)
        {
            letterButtons[i].Activate();
        }
    }

    private void Reread()
    {
        formedWord = string.Empty;
        for (int i = 0; i < answerBlanks.Length; i++)
        {
            if (answerBlanks[i].MyText == null || answerBlanks[i].IsConstant())
            {
                continue;
            }
            if (answerBlanks[i].MyText.text != string.Empty)
            {
                formedWord += answerBlanks[i].MyText.text;
            }
        }
    }

    public void OpenClose()
    {
        blur.SetActive(true);
        //blur.SetActive(true? false : true);
        canvasGroup.alpha = canvasGroup.alpha > 0 ? 0 : 1;
        canvasGroup.blocksRaycasts = canvasGroup.blocksRaycasts == true ? false : true;
        canvasGroup.interactable = canvasGroup.interactable == true ? false : true;
    }
    public IEnumerator Hourglass()
    {
        float tmp = myTimer;
        while (tmp > 0)
        {
            if(tmp <= 5)
            {
                timerText.color = Color.red;
                SetTickSound(true);
                //timerIcon.GetComponent<Image>().color = Color.red;
            }
            timerText.text = tmp.ToString("F0");
            tmp -= 1 * Time.deltaTime;
            yield return null;
        }

        StartCoroutine(Wrong());
    }

    public void SetTickSound(bool value)
    {
        
        if(value == true && currentTickStatus == false)
        {
            currentTickStatus = true;
            SFXMgr.MyInstance.PlaySound("TickSound");
        } 
        else if (value == false)
        {
            currentTickStatus = false;
            SFXMgr.MyInstance.StopSound("TickSound");
        }  
    }

    public void Resume()
    {
        currentWord.SetBlankedWord(currentWord.MyWord);
        endSentence.text = "";
        if (sevenEightWord)
        {
            answerBlanks[5].transform.SetParent(answerBlanks[1].transform.parent);
            answerBlanks[6].transform.SetParent(answerBlanks[1].transform.parent);
            for (int i = answerBlanks.Length/2; i < answerBlanks.Length; i++)
            {
                answerBlanks[i].transform.SetParent(transform);
                answerBlanks[i].transform.SetParent(secondRowAnswer.transform);
            }
        }
        sevenEightWord = false;
        charRemaining.Clear();
        charDeleted.Clear();
        Destroy(currentAudio.gameObject);
        comboMultiplier = TotalCombo();
        multiplierText.text = comboMultiplier.ToString() + "x";
        OpenClose();
        blur.SetActive(false);
        //StopAllCoroutines();
        GameManager.Instance.Resume();
        formedWord = string.Empty;
        for (int i = 0; i < answerBlanks.Length; i++)
        {
            if (answerBlanks[i].MyText == null)
            {
                break;
            }

            secondRowAnswer.SetActive(false);
            answerBlanks[i].Reset();
            answerBlanks[i].Deactivate();
        }
        for (int i = 0; i < letterButtons.Length; i++)
        {
            letterButtons[i].Activate();
        }
    }
    private IEnumerator Correct()
    {
        if (isCarrot)
        {
            GameManager.Instance.CarrotSkin();
        }

        SetTickSound(false);
        consecutive++;
        timerText.color = Color.white;
        //timerIcon.GetComponent<Image>().color = Color.white;
        GameManager.Instance.ChangeDifficulty(consecutive);
        health.CheckPoints(pointsToEarn);
        SFXMgr.MyInstance.PlaySound("Correct");
        StartCoroutine(showWord());
        correctWrongText.color = Color.green;
        correctWrongText.text = "CORRECT";
        StopCoroutine(hourglass);
        GameManager.Instance.AddPoints(pointsToEarn, consecutive);
        GameManager.Instance.AddAchieveDiff(currentWord.MyDifficulty);
        checkMark.SetActive(true);
        checkMark.GetComponentInChildren<Animator>(true).SetTrigger("Play");
        yield return new WaitForSeconds(wordTimeLength());
        endShowWord.text = "";
        correctWrongText.text = "";
        checkMark.SetActive(false);
        GameManager.Instance.Grab();
        Resume();
        if(GameManager.Instance.isTutorial)
        {
            OnCorrect?.Invoke();
        }
    }

    private IEnumerator Wrong()
    {
        SetTickSound(false);
        SFXMgr.MyInstance.PlaySound("Wrong");
        timerText.color = Color.white;
        //timerIcon.GetComponent<Image>().color = Color.white;
        GameManager.Instance.ResetDifficulty();
        StartCoroutine(showWord());
        correctWrongText.color = Color.red;
        correctWrongText.text = "TIME'S UP!";
        wrongMark.SetActive(true);
        wrongMark.GetComponentInChildren<Animator>(true).SetTrigger("Play");
        yield return new WaitForSeconds(wordTimeLength());
        endShowWord.text = "";
        correctWrongText.text = "";
        endSentence.text = "";
        consecutive = 0;
        wrongMark.SetActive(false);
        health.Damage();
        yield return new WaitForSeconds(.5f);
        Resume();
        myCamera.ShakeScreen();
        GameManager.Instance.Fail();
        if (GameManager.Instance.isTutorial)
        {
            OnWrong?.Invoke();
        }
    }

    private float wordTimeLength()
    {
        float tmp = 0;
        //for (int i = 0; i < currentWord.MyWord.Length; i++)
        //{
        //    tmp += showingWordSpeed;
        //}

        tmp += .8f;
        tmp += showingWordSpeed;
        return tmp;
    }
    private IEnumerator showWord()
    {
        PlayAudio();
        string slowShow = "";
        endShowWord.text = slowShow;
        currentWord.SetBlankedWord(currentWord.MyWord);
        endSentence.text = currentWord.MySentence;

        for (int i = 0; i < currentWord.MyWord.Length; i++)
        {
            slowShow += currentWord.MyWord[i];
            endShowWord.text = slowShow;
            yield return new WaitForSeconds(showingWordSpeed/currentWord.MyWord.Length);
        }
    }

    private IEnumerator addComboAnimation()
    {
        addEffect.SetTrigger("Play");
        GameObject multEffect = Instantiate(multiplierEffect, multiplierText.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(.5f);
        SFXMgr.MyInstance.PlaySound("AddPoints");
        multiplierText.fontSize = 149;
        multiplierText.text = comboMultiplier.ToString() + "x";
        //Destroy(multEffect, 1f);
        yield return new WaitForSeconds(1);
        multiplierText.fontSize = 59;
    }
    private int TotalCombo()
    {
        if (consecutive == 30 || consecutive == 14 || consecutive == 6 || consecutive == 2)
        {
            if(GameManager.Instance.isTutorial)
            {
                OnMultiplierUp?.Invoke();
            }
            StartCoroutine(addComboAnimation());
        }
        if (consecutive >= 30)
        {
            return 16;
        }
        if (consecutive >= 14)
        {
            return 8;
        }
        if (consecutive >= 6)
        {
            return 4;
        }
        if (consecutive >= 2)
        {
            return 2;
        }
        return 1;
    }

    public void PlayAudio()
    {
        if (currentAudio!=null)
        {
            currentAudio.Play();
        }
    }

    public void Hint()
    {
        SFXMgr.MyInstance.PlaySound("Hint");
        if (hintUses > 0)
        {
            AddBlanks();
            sentenceText.text = currentWord.MySentence;
            hintUses--;
            hintDisplayUses.text = hintUses.ToString();
            Reread();
            CheckWord();

            AddSentence();
            sentenceText.text = currentWord.MySentence;

            if (hintUses <= 0)
            {
                hintDisplayUses.GetComponentInParent<Button>().interactable = false;
            }
        }
    }

    public void IsCarrot (bool isCarrot)
	{
        this.isCarrot = isCarrot;
	}
}
