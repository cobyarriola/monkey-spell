using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrees : MonoBehaviour
{
    private float moveSpeed;

    [SerializeField]
    private float range;

    private float moveTime;

    private Vector3 target;

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, moveSpeed * Time.fixedDeltaTime);
    }

    public void ChangeRange(float newRange, Vector3 newTarget)
    {
        target = new Vector3(transform.position.x, transform.position.y - newRange, transform.position.z);
        moveSpeed = newRange;
        moveTime = 1;
    }
}
