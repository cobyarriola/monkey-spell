using UnityEngine;

public class FullScreenSprite : MonoBehaviour
{
    [SerializeField]
    private Camera myCamera;

    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private bool adjustHeight;

    [SerializeField]
    private bool adjustWidth;
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        //myCamera = FindObjectOfType<Camera>();
        ResizeSpriteToScreen(1, 1);
    }
    void ResizeSpriteToScreen(int fitToScreenWidth, int fitToScreenHeight)
    {
        transform.localScale = new Vector3(1, 1, 1);

        float width = spriteRenderer.sprite.bounds.size.x;
        float height = spriteRenderer.sprite.bounds.size.y;

        float worldScreenHeight = (float)(myCamera.orthographicSize * 2.0);
        float worldScreenWidth = (float)(worldScreenHeight / Screen.height * Screen.width);

        if (fitToScreenWidth != 0 && adjustWidth)
        {
            Vector2 sizeX = new Vector2(worldScreenWidth / width / fitToScreenWidth, transform.localScale.y);
            transform.localScale = sizeX;
        }

        if (fitToScreenHeight != 0 && adjustHeight)
        {
            Vector2 sizeY = new Vector2(transform.localScale.x, worldScreenHeight / height / fitToScreenHeight);
            transform.localScale = sizeY;
        }
    }
}