using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum GradeLevel { Grade1, Grade2, Grade3, Grade4, Grade5, Grade6}
public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }

            return instance;
        }
    }

    public delegate void ClimbTree();
    public static event ClimbTree OnClimb;

    public delegate void BananaClicked();
    public BananaClicked OnBananaClicked;

    public delegate void OnTutorialStart();
    public OnTutorialStart TutorialStarted;
    
    [SerializeField]
    private Banana currentBanana;

    [SerializeField]
    public List<Tree> trees;

    [SerializeField]
    private Monke myMonke;

    [SerializeField]
    private MoveTrees moveTrees;

    [SerializeField]
    private GameObject tree;

    private bool isClimbing;

    //[HideInInspector]
    public WordDatabase easyDB, moderateDB, hardDB;

    private int points = 0;

    private int spelledWords = 0;

    private int longestStreak = 0;

    private int speltEasy = 0, speltModerate = 0, speltHard = 0;

    [SerializeField]
    private Text pointsText;

    [SerializeField]
    private Text gameOverPoints;
    [SerializeField]
    private Text gameOverSpelt;

    [SerializeField]
    private CanvasGroup gameOverLayover;

    [SerializeField]
    private CanvasGroup healthTab;

    [SerializeField]
    private GameObject settingsButton;

    [SerializeField]
    private Stars stars;

    [SerializeField]
    private List<ScrollingBG> scrollers;

    [SerializeField]
    private GradeLevel gradeLevel;
    public GradeLevel MyGradeLevel
    {
        get
        {
            return gradeLevel;
        }
    }

    public AudioSource backgroundMusic;

    [SerializeField]
    private int easyBaseChance, moderateBaseChance, hardBaseChance;

    private int easyChance, moderateChance, hardChance;

    [SerializeField]
    private Text streakText;

    public bool isTutorial;

    private float yTargetReduction;

    public void Start()
    {
        StartCoroutine(PlayJungleAmbient());
        if(isTutorial)
        {
            TutorialStarted?.Invoke();
            //Debug.Log(TutorialStarted.)
        }
    }

    IEnumerator PlayJungleAmbient()
    {
        yield return new WaitForSeconds(1);
        SFXMgr.MyInstance.PlaySound("JungleAmbient");
        SFXMgr.MyInstance.SetVolume("JungleAmbient");
    }

    public void SetYTarget(float newY)
    {
        yTargetReduction = newY;
    }

    public void Update()
    {   
        if(EventSystem.current.IsPointerOverGameObject()) return;

        if (Input.GetMouseButtonDown(0) && !isClimbing)
        {
            //StopAllCoroutines();
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider == null)
            {
                return;
            }
            if (hit.collider.CompareTag("Banana"))
            {
                isClimbing = true;
                currentBanana = hit.collider.transform.GetComponent<Banana>();
                Climb();
                
                if(isTutorial)
                {             
                    OnBananaClicked?.Invoke();                  
                }

                if (OnClimb != null)
                {
                    OnClimb();
                }
            }
        }
    }

    public void SettingsPaused()
    {
        backgroundMusic.Stop();
        SFXMgr.MyInstance.PauseSounds("Select");
        //SFXMgr.MyInstance.StopAllSounds();
        stars.Show(points);
        isClimbing = true;
    }
    public void SettingsResume()
    {
        backgroundMusic.Play();
        SFXMgr.MyInstance.PlaySound("Select");
        SFXMgr.MyInstance.Unpause();
        //SFXMgr.MyInstance.PlaySound("JungleAmbient");
        isClimbing = false;
        stars.OpenCloseStars();
    }

    public void SetGradeLevel(GradeLevel level)
    {
        gradeLevel = level;
    }

    public void AddWordDatabase(WordDatabase newDB, Difficulty difficulty)
    {
        if (difficulty == Difficulty.Easy)
        {
            easyDB = newDB;
        }
        if (difficulty == Difficulty.Moderate)
        {
            moderateDB = newDB;
        }
        if (difficulty == Difficulty.Hard)
        {
            hardDB = newDB;
        }
    }

    private void Climb()
    {
        SFXMgr.MyInstance.PlaySound("Jump");
        for (int i = 0; i < scrollers.Count; i++)
        {
            scrollers[i].ChangeRange(currentBanana.transform.position.y - myMonke.transform.position.y - 1f);
            scrollers[i].StartCoroutine(scrollers[i].MoveDownwards());
        }

        moveTrees.ChangeRange(currentBanana.transform.position.y - myMonke.transform.position.y - yTargetReduction, currentBanana.transform.position);
        myMonke.ChangeTarget(currentBanana.transform);
        myMonke.JumpAnimation();
        StartCoroutine(SpellingZone());
    }

    public void AddScroller(ScrollingBG bg)
    {
        scrollers.Add(bg);
    }

    public void SpawnTree(Transform lastTree)
    {
        if (trees.Count > 2)
        {
            Destroy(trees[0].gameObject);
            trees.RemoveAt(0);
        }

        GameObject newTree = Instantiate(tree, new Vector3(lastTree.position.x, lastTree.position.y + 9.45f, 0), Quaternion.identity, moveTrees.transform);
        trees.Add(newTree.GetComponent<Tree>());
    }

    private IEnumerator SpellingZone()
    {
        yield return new WaitForSeconds(2);
        //settingsButton.SetActive(false);
        settingsButton.GetComponent<Button>().interactable = false;
        SpellZone.Instance.ChangeWord(getWord());
        SpellZone.Instance.IsCarrot(currentBanana.IsCarrot);
        SpellZone.Instance.OpenClose();
        //OpenClose(healthTab);
    }

    private Word getWord()
    {
        if (currentBanana.MyDifficulty == Difficulty.Easy)
        {
            return easyDB.MyWords[Random.Range(0, easyDB.MyWords.Length)];
        }
        if (currentBanana.MyDifficulty == Difficulty.Moderate)
        {
            return moderateDB.MyWords[Random.Range(0, moderateDB.MyWords.Length)];
        }
        if (currentBanana.MyDifficulty == Difficulty.Hard)
        {
            return hardDB.MyWords[Random.Range(0, hardDB.MyWords.Length)];
        }
        return null;
    }
    public void Resume()
    {
        //OpenClose(healthTab);
        settingsButton.SetActive(true);
        settingsButton.GetComponent<Button>().interactable = true;
        Destroy(currentBanana.gameObject);
        isClimbing = false;
    }

    public IEnumerator GameOver()
    {
        settingsButton.SetActive(false);
        gameOverPoints.text = points.ToString();
        gameOverSpelt.text = spelledWords.ToString();
        streakText.text = longestStreak.ToString();
        StartCoroutine(myMonke.Death());
        GameManager.Instance.backgroundMusic.Stop();
        SFXMgr.MyInstance.StopAllSounds("Select");
        SFXMgr.MyInstance.PlaySound("Gameover");
        yield return new WaitForSeconds(3);
        if(!isTutorial)
        {
            SaveData();
            UpdateDaily();
        }
        Time.timeScale = 0;
        settingsButton.SetActive(false);
        OpenClose(healthTab);
        OpenClose(gameOverLayover);
        stars.Show(points);
    }

    private void SaveData()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        int index = (int)gradeLevel;
        if (dataManager.data.GetData(index).stars < stars.GetNumberOfStars())
        {
            dataManager.data.GetData(index).stars = stars.GetNumberOfStars();

            int allStars = 0;
            for (int i = 0; i < dataManager.data.myGradeData.Length; i++)
            {
                allStars += dataManager.data.GetData(i).stars;
            }
            dataManager.data.GetAchieveWrite("AllStars").value = allStars;
        }
        if (dataManager.data.GetData(index).highscore < points)
        {
            dataManager.data.GetData(index).highscore = points;

            if (dataManager.data.GetAchieveWrite("HighestScore").value < points)
            {
                dataManager.data.GetAchieveWrite("HighestScore").value = points;
            }
        }
        if (dataManager.data.GetData(index).highwords < spelledWords)
        {
            dataManager.data.GetData(index).highwords = spelledWords;
        }
        if (dataManager.data.GetData(index).streak < longestStreak)
        {
            dataManager.data.GetData(index).streak = longestStreak;

            if (dataManager.data.GetAchieveWrite("Streak").value < longestStreak)
            {
                dataManager.data.GetAchieveWrite("Streak").value = longestStreak;
            }
        }

        dataManager.data.GetAchieveWrite("TotalScore").value += points;
        dataManager.data.GetAchieveWrite("TotalSpell").value += spelledWords;
        dataManager.data.GetAchieveWrite("SpellEasy").value += speltEasy;
        dataManager.data.GetAchieveWrite("SpellModerate").value += speltModerate;
        dataManager.data.GetAchieveWrite("SpellHard").value += speltHard;

        dataManager.Save();
    }

    private void UpdateDaily()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        for (int i = 0; i < dataManager.data.DailyLength(); i++)
        {
            if (dataManager.data.GetDailies(i).claimed == true)
            {
                continue;
            }

            if (dataManager.data.GetDailies(i).id == "GetStreak")
            {
                if (dataManager.data.GetDailies(i).value < longestStreak)
                {
                    dataManager.data.GetDailies(i).value = longestStreak;
                }
            }
            if (dataManager.data.GetDailies(i).id == "GetSpell")
            {
                dataManager.data.GetDailies(i).value += spelledWords;
            }
            if (dataManager.data.GetDailies(i).id == "GetSpellEasy")
            {
                dataManager.data.GetDailies(i).value += speltEasy;
            }
            if (dataManager.data.GetDailies(i).id == "GetSpellModerate")
            {
                dataManager.data.GetDailies(i).value += speltModerate;
            }
            if (dataManager.data.GetDailies(i).id == "GetSpellHard")
            {
                dataManager.data.GetDailies(i).value += speltHard;
            }
            if (dataManager.data.GetDailies(i).id == "GetPoints")
            {
                dataManager.data.GetDailies(i).value += points;
            }
            if (dataManager.data.GetDailies(i).id == "Get3Stars")
            {
                dataManager.data.GetDailies(i).value = stars.GetNumberOfStars();
            }
        }
        dataManager.Save();
    }

    public void AddPoints(int newPoints, int consecutive)
    {
        if (consecutive > longestStreak)
        {
            longestStreak = consecutive;
        }

        spelledWords++;
        points += newPoints;
        pointsText.text = points.ToString();
        stars.EffectStars(points);
    }

    public void AddAchieveDiff(Difficulty diff)
    {
        if (diff == Difficulty.Easy)
        {
            speltEasy += 1;
        }
        if (diff == Difficulty.Moderate)
        {
            speltModerate += 1;
        }
        if (diff == Difficulty.Hard)
        {
            speltHard += 1;
        }
    }

    private void Awake()
    {
        ResetDifficulty();
        Time.timeScale = 1;
        pointsText.text = points.ToString();
        gameOverPoints.text = points.ToString();
        backgroundMusic = GetComponentInChildren<AudioSource>();
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SFXMgr.MyInstance.PlaySound("Select");
        SFXMgr.MyInstance.StopAllSounds("Select");
    }

    public void OpenClose(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = canvasGroup.alpha > 0 ? 0 : 1;
        canvasGroup.blocksRaycasts = canvasGroup.blocksRaycasts == true ? false : true;
        canvasGroup.interactable = canvasGroup.interactable == true ? false : true;
    }

    public Difficulty BananasChance()
    {
        int tmp = Random.Range(0, easyChance + moderateChance + hardChance);

        if (tmp < easyChance)
        {
            return Difficulty.Easy;
        }
        else if (tmp < moderateChance + easyChance)
        {
            return Difficulty.Moderate;
        }
        else if (tmp < easyChance + moderateChance + hardChance)
        {
            return Difficulty.Hard;
        }

        return Difficulty.Easy;
    }

    public void ChangeDifficulty(int consecutive)
    {
        if (easyChance <= 20)
        {
            if (moderateChance > 30)
            {
                moderateChance -= 2;
                hardChance += 2;
            }
            return;
        }
        if (consecutive % 2 == 0)
        {
            easyChance -= 2;
            moderateChance += 2;
        }
        else
        {
            easyChance -= 1;
            hardChance += 1;
        }
    }

    public void ResetDifficulty()
    {
        easyChance = easyBaseChance;
        moderateChance = moderateBaseChance;
        hardChance = hardBaseChance;
    }

    public Vector2 GetMonkePos()
    {
        return new Vector2(myMonke.transform.position.x, myMonke.transform.position.y);
    }

    public void RemoveScroller(ScrollingBG bg)
    {
        scrollers.Remove(bg);
    }

    public void SetStarValues(int first, int second, int third)
    {
        stars.SetStarValues(first, second, third);
    }

    public void CarrotSkin()
    {
       //  myMonke.ChangeToCarrot();
        PlayerDataManager.Instance.data.mySkins[6] = "RegMonke1";
        PlayerDataManager.Instance.Save();
    }

    public void Grab()
    {
        myMonke.Grab();
    }
    public void Fail()
    {
        myMonke.Fail();
    }
}