using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum Difficulty { Easy, Moderate, Hard }

[CreateAssetMenu(fileName = "Word", menuName = "Words/Word", order = 2)]
public class Word : ScriptableObject
{
    [SerializeField]
    private Difficulty myDifficulty;
    internal Difficulty MyDifficulty
    {
        get
        {
            return myDifficulty;
        }
    }

    [SerializeField]
    private string word;
    public string MyWord
    {
        get
        {
            return word.ToUpper();
        }
    }

    [SerializeField]
    private Sprite myImage;
    public Sprite MyImage
    {
        get
        {
            return myImage;
        }
    }


    [SerializeField]
    private AudioSource myAudio;
    public AudioSource MyAudio
    {
        get
        {
            return myAudio;
        }
    }

    private string mySentence;
    public string MySentence
    {
        get
        {
            return string.Format("{0}{1}{2}", beforeBlank, blankedWord, afterBlank);
        }
    }

    [SerializeField]
    private string beforeBlank;

    [SerializeField]
    private string afterBlank;

    private string blankedWord;

    public void SetBlankedWord(string newWord)
    {
        blankedWord = newWord;
    }
}
