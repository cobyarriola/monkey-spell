using System;
using UnityEngine;
using UnityEngine.UI;

public class Stars : MonoBehaviour
{

    [SerializeField]
    private Sprite fillStar, noFillStar;

    private Image[] stars;
    private Text[] pointsRequireds;

    private CanvasGroup myCanvasGroup;

    private int oneStar, twoStars, threeStars;

    [SerializeField]
    private GameObject starEffect;

    [SerializeField]
    private GameObject starParticles;

    [SerializeField]
    private GameObject starLocation;

    private bool firstStar, secondStar, thirdStar;

    private int noOfStars;

    private void Start()
    {
        myCanvasGroup = GetComponentInParent<CanvasGroup>();
        stars = GetComponentsInChildren<Image>();

        if(GameManager.Instance.isTutorial)
        {
            SetStarValues(100, 200, 300);
        }
    }

    public void SetStarValues(int first, int second, int third)
    {
        pointsRequireds = GetComponentsInChildren<Text>();

        oneStar = first;
        twoStars = second;
        threeStars = third;

        pointsRequireds[0].text = oneStar.ToString();
        pointsRequireds[1].text = twoStars.ToString();
        pointsRequireds[2].text = threeStars.ToString();
    }

    public void Show(int points)
    {
        if (points >= threeStars)
        {
            ActivateStar(3);
            noOfStars = 3;
        }
        else if (points >= twoStars)
        {
            ActivateStar(2);
            noOfStars = 2;
        }
        else if (points >= oneStar)
        {
            ActivateStar(1);
            noOfStars = 1;
        }
        else
        {
            ActivateStar(0);
            noOfStars = 0;
        }
        OpenCloseStars();
    }

    private void ActivateStar(int activating)
    {

        for (int i = 0; i < stars.Length; i++)
        {
            if (i < activating)
            {
                stars[i].sprite = fillStar;
                continue;
            }

            stars[i].sprite = noFillStar;
        }
    }
    public void EffectStars(int points)
    {
        if (points >= oneStar && !firstStar)
        {
            SFXMgr.MyInstance.PlaySound("Star");
            GameObject starEff = Instantiate(starEffect, starLocation.transform.position, Quaternion.identity, starLocation.transform);
            GameObject starParts = Instantiate(starParticles, starLocation.transform.position, Quaternion.identity, starLocation.transform);
            Destroy(starEff, 2f);
            Destroy(starParts, 1.8f);
            ActivateStar(1);
            firstStar = true;
        }
        if (points >= twoStars && !secondStar)
        {
            SFXMgr.MyInstance.PlaySound("Star");
            GameObject starEff = Instantiate(starEffect, starLocation.transform.position, Quaternion.identity, starLocation.transform);
            GameObject starParts = Instantiate(starParticles, starLocation.transform.position, Quaternion.identity, starLocation.transform);
            Destroy(starEff, 2f);
            Destroy(starParts, 1.8f);
            ActivateStar(2);
            secondStar = true;
        }
        if (points >= threeStars && !thirdStar)
        {
            SFXMgr.MyInstance.PlaySound("Star");
            GameObject starEff = Instantiate(starEffect, starLocation.transform.position, Quaternion.identity, starLocation.transform);
            GameObject starParts = Instantiate(starParticles, starLocation.transform.position, Quaternion.identity, starLocation.transform);
            Destroy(starEff, 2f);
            Destroy(starParts, 1.8f);
            ActivateStar(3);
            thirdStar = true;
        }
        else
        {
            ActivateStar(0);
        }
    }

    public void OpenCloseStars()
    {
        myCanvasGroup.alpha = myCanvasGroup.alpha > 0 ? 0 : 1;
        myCanvasGroup.blocksRaycasts = myCanvasGroup.blocksRaycasts == true ? false : true;
        myCanvasGroup.interactable = myCanvasGroup.interactable == true ? false : true;
    }

    public int GetNumberOfStars()
    {
        return noOfStars;
    }
}
