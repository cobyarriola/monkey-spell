using System.Collections;
using UnityEngine;
using Spine.Unity;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] clouds;

    [SerializeField]
    private GameObject bird;

    private int direction;

    private void Start()
    {
        StartCoroutine(spawnClouds());
        StartCoroutine(spawnBirds());
    }
    private IEnumerator spawnClouds()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            GameObject cloud = Instantiate(clouds[Random.Range(0, clouds.Length)], RandomLocation(), Quaternion.identity, transform.parent.parent);
            cloud.GetComponent<ObjectMove>().SetDirection(direction);
        }
    }

    private Vector3 RandomLocation()
    {
        int rng = Random.Range(1, 3);
        if (rng == 1)
        {
            direction = 1;
            return new Vector3(transform.position.x - 5, transform.position.y + Random.Range(-11, 9), 0);
        }
        direction = -1;
        return new Vector3(transform.position.x + 5, transform.position.y + Random.Range(-11, 9), 0);

    }
    private IEnumerator spawnBirds()
    {
        yield return new WaitForSeconds(1f);

        while (true)
        {
            int rng = Random.Range(0, 4);
            GameObject newBird = Instantiate(bird, RandomLocation(), Quaternion.identity, transform.parent.parent);
            ObjectMove birdMover = newBird.GetComponent<ObjectMove>();
            birdMover.SetDirection(direction);

            float size = Random.Range(.5f, 1f);
            newBird.transform.localScale = new Vector3(size * direction, size, 0);
            SkeletonAnimation birdSkeleton = newBird.GetComponent<SkeletonAnimation>();

            switch (rng)
            {
                case 0:
                    birdSkeleton.skeleton.SetSkin("Bird");
                    birdSkeleton.AnimationName = "Flight Simulations Bird";
                    birdMover.SetSpeed(1f);
                    break;
                case 1:
                    birdSkeleton.skeleton.SetSkin("Bird2");
                    birdSkeleton.AnimationName = "Flight Simulations Bird2";
                    birdMover.SetSpeed(.6f);
                    break;
                case 2:
                    birdSkeleton.skeleton.SetSkin("Bird3");
                    birdSkeleton.AnimationName = "Flight Simulations Bird3";
                    birdMover.SetSpeed(.4f);
                    break;
                case 3:
                    birdSkeleton.skeleton.SetSkin("Bird4");
                    birdSkeleton.AnimationName = "Flight Simulations Bird4";
                    birdMover.SetSpeed(.8f);
                    break;
                default:
                    break;
            }

            birdSkeleton.skeleton.SetSlotsToSetupPose();

            yield return new WaitForSeconds(Random.Range(12f, 18f));
        }
    }

}
