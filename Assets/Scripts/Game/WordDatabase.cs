using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WordDatabase : MonoBehaviour
{
    public Word[] MyWords;

    [SerializeField]
    private float timeGiven;

    [SerializeField]
    private Difficulty difficulty;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.AddWordDatabase(this, difficulty);
            SpellZone.Instance.SetTimer(timeGiven, difficulty);
        }
    }
}
