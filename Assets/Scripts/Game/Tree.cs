using UnityEngine;

public class Tree : MonoBehaviour
{
    [SerializeField]
    private bool firstTree;

    [SerializeField]
    private GameObject[] branches;

    [SerializeField]
    private GameObject[] bananas;

    private bool hasSpawned;
    private void Start()
    {
        SpawnBranches();
    }

    private void SpawnBranches()
    {
        for (int i = 0; i < 3; i++)
        {
            int rand = Random.Range(0, branches.Length);
            float xPos = 0;
            switch (rand)
            {
                case 0:
                    xPos = 1.5f;
                    break;
                case 1:
                    xPos = 1.5f;
                    break;
                case 2:
                    xPos = -1.4f;
                    break;
                case 3:
                    xPos = -1.14f;
                    break;
                default:
                    break;
            }

            if (firstTree)
            {
                if (i > 1)
                {
                    break;
                }
                Instantiate(branches[rand], new Vector3(xPos, -1 + (3 * i) + transform.position.y, 0), Quaternion.identity, transform);
                //Instantiate(bananas[Random.Range(0, bananas.Length)], new Vector3(xPos, .5f + (3 * i) + transform.position.y, 0), Quaternion.identity, transform);
                SpawnBananas(xPos, .5f, i);
                continue;
            }

            Instantiate(branches[rand], new Vector3(xPos, -4+(3*i) + transform.position.y, 0), Quaternion.identity, transform);
            SpawnBananas(xPos, -2.5f, i);
        }
    }

    private void SpawnBananas(float xPos, float yPos, int iteration)
    {
        if (KevinTheCarrot(xPos, yPos, iteration))
        {
            return;
        }

        int index = -1;
        Difficulty rngDifficulty = GameManager.Instance.BananasChance();
        for (int i = 0; i < bananas.Length - 1; i++)
        {
            if (rngDifficulty == bananas[i].GetComponent<Banana>().MyDifficulty)
            {
                index = i;
            }
        }

        Instantiate(bananas[index], new Vector3(xPos, yPos + (3 * iteration) + transform.position.y - .1f, 0), Quaternion.identity, transform);
    }

    private bool KevinTheCarrot(float xPos, float yPos, int iteration)
    {
        int rng = Random.Range(0, 100);
        if (rng <= 1)
        {
            Instantiate(bananas[3], new Vector3(xPos, yPos + (3 * iteration) + transform.position.y - .1f, 0), Quaternion.identity, transform);
            return true;
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !hasSpawned)
        {
            hasSpawned = true;
            GameManager.Instance.SpawnTree(transform);
            //GameManager.Instance.trees.Add(newTree.GetComponent<Tree>());
        }
    }
    
}
