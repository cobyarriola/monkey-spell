using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour
{
    private int direction = 1;

    private float speed = .5f;
    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 newPos = new Vector3(transform.position.x + 1 * direction, transform.position.y, 0);
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * speed);

        if (transform.position.x > 10 || transform.position.x < -10)
        {
            Destroy(transform.gameObject);
        }
    }

    public void SetDirection(int dir)
    {
        direction = dir;
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    private void Start()
    {
        speed = Random.Range(.2f, 1.1f);
    }
}
