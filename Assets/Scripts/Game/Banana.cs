using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Banana : MonoBehaviour
{
    [SerializeField] 
    private bool isCarrot;
    internal bool IsCarrot
    {
        get
        {
            return isCarrot;
        }
    }

    [SerializeField]
    private Difficulty myDifficulty;

    internal Difficulty MyDifficulty
    {
        get
        {
            return myDifficulty;
        }
    }
}
