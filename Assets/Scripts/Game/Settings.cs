using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
    private CanvasGroup canvasGroup;

    [SerializeField]
    private CanvasGroup promptBox;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Resume()
    {
        GameManager.Instance.OpenClose(canvasGroup);
        Time.timeScale = 1;
        GameManager.Instance.SettingsResume();
    }

    public void Prompt()
    {
        GameManager.Instance.OpenClose(promptBox);
        SFXMgr.MyInstance.PlaySound("Select");
    }

    public void ExitTutorial()
    {
        Time.timeScale = 1;
        //SFXMgr.MyInstance.PlaySound("Select");
        //Resume();
        //Prompt();
        //GameManager.Instance.backgroundMusic.Stop();
        SFXMgr.MyInstance.StopAllSounds("Select");
        //SFXMgr.MyInstance.PlaySound("Gameover");
        //StartCoroutine(GameManager.Instance.GameOver());
        BackToLevelSelect();

        PlayerDataManager.Instance.data.finishedTutorial = true;
        PlayerDataManager.Instance.Save();
    }

    public void EndGame()
    {
        Time.timeScale = 1;
        SFXMgr.MyInstance.PlaySound("Select");
        Resume();
        Prompt();
        GameManager.Instance.backgroundMusic.Stop();
        SFXMgr.MyInstance.StopAllSounds("Select");
        SFXMgr.MyInstance.PlaySound("Gameover");
        StartCoroutine(GameManager.Instance.GameOver());
    }

    public void BackToMenu()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        SFXMgr.MyInstance.StopAllSounds("Select");
        SceneManager.LoadScene("MainMenu");
        if(!GameManager.Instance.isTutorial)
        {
            DataManager.Instance.Destroy();
        }       
    }

    public void BackToLevelSelect()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        SFXMgr.MyInstance.StopAllSounds("Select");
        DataManager.Instance.backToLevelSelect = true;
        SceneManager.LoadScene("MainMenu");

        if(!GameManager.Instance.isTutorial)
        {
            DataManager.Instance.Destroy();    
        }  
    }

    public void OpenSettings()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        GameManager.Instance.OpenClose(canvasGroup);
        Time.timeScale = 0;
        GameManager.Instance.SettingsPaused();
    }
}
