using System.Collections;
using UnityEngine;
using Spine.Unity;

public class Monke : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    //private Animator anim;

    [SerializeField]
    private float moveSpeed;

    private bool deathMove;

    [SerializeField]
    private SkeletonAnimation skeleton;

    [SerializeField]
    private AnimationReferenceAsset idle;

    [SerializeField]
    private AnimationReferenceAsset fall;

    [SerializeField]
    private AnimationReferenceAsset jump;

    [SerializeField]
    private AnimationReferenceAsset grab;

    [SerializeField]
    private AnimationReferenceAsset fail;

    [SerializeField]
    private float yTargetReduction;

    private void Awake()
    {
        SetAnimation(idle, true, 1f);
        if(!GameManager.Instance.isTutorial)
        {
            skeleton.skeleton.SetSkin(PlayerDataManager.Instance.data.equippedSkin);
        }
    }

    public void ChangeToCarrot()
    {
        skeleton.skeleton.SetSkin("RegMonke1");
        skeleton.skeleton.SetSlotsToSetupPose();
    }

    public void ChangeTarget(Transform newTarget)
    {
        target = newTarget;
    }

    private void FixedUpdate()
    {
        if (target != null)
        {
            FollowTarget();
        }

        if (deathMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.down, moveSpeed * Time.deltaTime);
        }
    }
    public void FollowTarget()
    {
        Vector3 newTarget = new Vector3(target.position.x - .1f, target.position.y - yTargetReduction, 0);
        GameManager.Instance.SetYTarget(yTargetReduction);
        //Vector3 loweredTarget = new Vector3(target.position.x, target.position.y -.2f, 0);
        transform.position = Vector3.MoveTowards(transform.position, newTarget, moveSpeed * Time.deltaTime);
    }

    public void JumpAnimation()
    {
        if (target.transform.position.x > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            //anim.SetFloat("xPos", 1);
        }
        else if (target.transform.position.x <= 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
            //anim.SetFloat("xPos", 0);
        }
        //anim.SetTrigger("Jump");
        SetAnimation(jump, false, 2f);
        StartCoroutine(ResumeIdle());
    }

    public IEnumerator Death()
    {
        SetAnimation(fall, false, 1f);
        //anim.SetTrigger("Death");
        yield return new WaitForSeconds(1f);
        deathMove = true;
    }

    private void SetAnimation(AnimationReferenceAsset animation, bool loop, float timeScale)
    {
        skeleton.AnimationState.SetAnimation(0, animation, loop).TimeScale = timeScale;
    }

    public void Grab()
    {
        SetAnimation(grab, false, 2f);
        StartCoroutine(ResumeIdle());
    }

    public void Fail()
    {
        SetAnimation(fail, false, 2f);
        StartCoroutine(ResumeIdle());
    }

    private IEnumerator ResumeIdle()
    {
        yield return new WaitForSeconds(1f);
        SetAnimation(idle, true, 1f);
    }
}
