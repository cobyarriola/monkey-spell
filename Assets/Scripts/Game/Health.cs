using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField]
    private GameObject[] hearts;

    [SerializeField]
    private int maxHealth;

    public int health;

    [SerializeField]
    private Sprite fullHeart, damagedHeart;

    [SerializeField]
    private int pointsToRegen;

    [SerializeField]
    private Image regenBar;

    private int accumulatedPoints;

    [SerializeField]
    private GameObject healEffect;

    [SerializeField]
    private GameObject healLocation;
    private void Awake()
    {
        InitHealth();
        accumulatedPoints = 0;
    }

    public void Damage()
    {
        hearts[health-1].GetComponent<Image>().sprite = damagedHeart;
        health -= 1;
        if (health <= 0)
        {
            StartCoroutine(GameManager.Instance.GameOver());
        }
    }

    private void InitHealth()
    {
        health = maxHealth;
        for (int i = 0; i < health; i++)
        {
            hearts[i].SetActive(true);
        }
    }

    public void CheckPoints(int points)
    {
        if (health == 3)
        {
            return;
        }

        if (accumulatedPoints < pointsToRegen)
        {
            accumulatedPoints += points;
        }

        if (accumulatedPoints >= pointsToRegen && health < 3)
        {
            accumulatedPoints = 0;

            if (health < 3)
            {
                Regenerate();
            }
        }

        float fill = (float)accumulatedPoints / (float)pointsToRegen;
        regenBar.fillAmount = fill;
    }

    private void Regenerate()
    {
        SFXMgr.MyInstance.PlaySound("Heal");
        hearts[health].GetComponent<Image>().sprite = fullHeart;
        GameObject healEff = Instantiate(healEffect, healLocation.transform.position, Quaternion.identity,healLocation.transform);
        //Destroy(healEff,2f);
        health += 1;
    }
}
