﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [Header("Main Menu UI")]
    public GameObject MainMenuPanel;

    [Header("Level Selection UI")]
    public GameObject LevelSelectionPanel;
    [SerializeField]
    private GameObject tutorialPrompt;

    [Header("Achievements UI")]
    public CanvasGroup AchievementsPanel;
    [SerializeField]
    private AchieveData achieveData;

    [Header("Shop UI")]
    public GameObject ShopPanel;
    [SerializeField]
    private Shop shop;

    [Header("Tutorial Confirmation UI")]
    public GameObject TutorialConfirmationPanel;

    [Header("Level Confirmation UI")]
    public GameObject LevelConfirmationPanel;
    public Text gradeLevelText;
    public Text highscore;
    public Text highestWords;
    public Text streak;
    [SerializeField]
    private Sprite filledStar, blankStar;
    [SerializeField]
    private Image[] stars;
    [SerializeField]
    private StarsSettings starsSettings;
    [SerializeField]
    private Text[] starTexts;

    [Header("Profile Screen UI")]
    public GameObject ProfileScreenPanel;
    public GameObject CreationScreen;

    [Header("Grade Database Selection")]
    public GameObject[] Grade1DB;
    public GameObject[] Grade2DB;
    public GameObject[] Grade3DB;
    public GameObject[] Grade4DB;
    public GameObject[] Grade5DB;
    public GameObject[] Grade6DB;
    GameObject[] db;



    public void PlayGame()
    {
        ActivatePanel(LevelSelectionPanel.name);
        SFXMgr.MyInstance.PlaySound("Select");

        if (PlayerDataManager.Instance.data.finishedTutorial == false)
        {
            tutorialPrompt.SetActive(true);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    // public void ClickedTutorialButton()
    // {
    //     ActivatePanel(TutorialConfirmationPanel.name);
    //     SFXMgr.MyInstance.PlaySound("Select");
    // }

    public void StartTutorial()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        SceneManager.LoadScene("Tutorial");
    }

    public void ClickedGradeButton(int gradeLevel)
    {
        SFXMgr.MyInstance.PlaySound("Select");
        ActivatePanel(LevelConfirmationPanel.name);
        gradeLevelText.text = "GRADE " + gradeLevel;

        // grade level added
        switch (gradeLevel)
        {
            case 1: db = Grade1DB; break;
            case 2: db = Grade2DB; break;
            case 3: db = Grade3DB; break;
            case 4: db = Grade4DB; break;
            case 5: db = Grade5DB; break;
            case 6: db = Grade6DB; break;

        }

        // db = Grade1DB;
        highscore.text = PlayerDataManager.Instance.data.GetData(gradeLevel-1).highscore.ToString();
        highestWords.text = PlayerDataManager.Instance.data.GetData(gradeLevel-1).highwords.ToString();
        streak.text = PlayerDataManager.Instance.data.GetData(gradeLevel-1).streak.ToString();
        ChangeStars(PlayerDataManager.Instance.data.GetData(gradeLevel-1).stars);

        int firstStar = starsSettings.GetGradeStar(gradeLevel - 1).oneStar;
        int secondStar = starsSettings.GetGradeStar(gradeLevel - 1).twoStars;
        int thirdStar = starsSettings.GetGradeStar(gradeLevel - 1).threeStars;
        starTexts[0].text = firstStar.ToString();
        starTexts[1].text = secondStar.ToString();
        starTexts[2].text = thirdStar.ToString();

        DataManager.Instance.ChangeGradeLevel((GradeLevel)gradeLevel-1);

        DataManager.Instance.ChangeStarValues(firstStar, secondStar, thirdStar);

        for (int i = 0; i < DataManager.Instance.db.Length; i++)
        {
            GameObject dbObj = Instantiate<GameObject>(db[i], transform.position, Quaternion.identity, DataManager.Instance.transform);       
            DataManager.Instance.SetDataBase(dbObj, i);
        }
    }

    public void ChangeStars(int val)
    {
        foreach (var image in stars)
        {
            image.sprite = blankStar;
        }

        for (int i = 0; i < val; i++)
        {
            stars[i].sprite = filledStar;
        }
    }

    public void ChangeProfileButton()
    {
        ActivatePanel(ProfileScreenPanel.name);
        SFXMgr.MyInstance.PlaySound("Select");
    }

    public void LevelSelectionBackButton()
    {    
        ActivatePanel(MainMenuPanel.name);
        DataManager.Instance.Destroy();
        SFXMgr.MyInstance.PlaySound("Select");
    }
    
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
        SFXMgr.MyInstance.PlaySound("Select");
    }

    public void LevelConfirmBackButton()
    {
        ActivatePanel(LevelSelectionPanel.name);
        SFXMgr.MyInstance.PlaySound("Select");
        DataManager.Instance.Destroy();
    }

    public void ProfileScreenBackButton()
    {
        ActivatePanel(MainMenuPanel.name);
        SFXMgr.MyInstance.PlaySound("Select");
    }

    public void OpenCloseCreationScreen() 
    {
        SFXMgr.MyInstance.PlaySound("Select");
        CreationScreen.SetActive(CreationScreen.activeSelf ? false : true);
        ProfileScreenPanel.SetActive(ProfileScreenPanel.activeSelf ? false : true);
    }

    public void OpenAchievements()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        AchievementsPanel.alpha = 1;
        AchievementsPanel.interactable = true;
        AchievementsPanel.blocksRaycasts = true;

        MainMenuPanel.SetActive(false);
        achieveData.UpdateValues();
    }

    public void AchievementsBackButton()
    {
        ActivatePanel(MainMenuPanel.name);
        SFXMgr.MyInstance.PlaySound("Select");
        AchievementsPanel.alpha = 0;
        AchievementsPanel.interactable = false;
        AchievementsPanel.blocksRaycasts = false;
    }
    public void OpenShop()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        ShopPanel.SetActive(true);
        shop.UpdateSkinsData();
    }

    void Start()
    {
        achieveData.UpdateValues();
        DailyMission.Instance.ProgressDaily();
        ActivatePanel(MainMenuPanel.name);
        
        if(DataManager.Instance.isLevelSelect())
        {
            ActivatePanel(LevelSelectionPanel.name);
            DataManager.Instance.backToLevelSelect = false;
        }
        //SFXMgr.MyInstance.PlaySound("Select");
        //DataManager.Instance.Destroy();
    }

    public void ActivatePanel(string panelName)
    {
        MainMenuPanel.SetActive(MainMenuPanel.name.Equals(panelName));
        LevelSelectionPanel.SetActive(LevelSelectionPanel.name.Equals(panelName));
        LevelConfirmationPanel.SetActive(LevelConfirmationPanel.name.Equals(panelName));
        ProfileScreenPanel.SetActive(ProfileScreenPanel.name.Equals(panelName));
        TutorialConfirmationPanel.SetActive(TutorialConfirmationPanel.name.Equals(panelName));
        ShopPanel.SetActive(ShopPanel.name.Equals(panelName));
    }
}