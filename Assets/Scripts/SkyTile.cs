using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyTile : MonoBehaviour
{
    private bool hasSpawned;

    [SerializeField]
    private GameObject sky;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !hasSpawned)
        {
            Vector3 pos = new Vector3(transform.position.x, transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y, 0);
            hasSpawned = true;
            Instantiate(sky, pos, Quaternion.identity, transform.parent);
            //GameManager.Instance.trees.Add(newTree.GetComponent<Tree>());
        }
    }
}
