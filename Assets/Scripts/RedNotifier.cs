using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedNotifier : MonoBehaviour
{
    private static RedNotifier instance;
    public static RedNotifier Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<RedNotifier>();
            }

            return instance;
        }
    }

    [SerializeField]
    private CanvasGroup dotMainMenu;

    [SerializeField]
    private CanvasGroup dotDailyTab;

    [SerializeField]
    private CanvasGroup dotAchTab;

    [SerializeField]
    private CanvasGroup dotLevelSelect;

    private int achCount;

    private int dailyCount;

    public void ClearAch()
    {
        achCount = 0;
    }
    public void ClearDaily()
    {
        dailyCount = 0;
    }

    public void HasClaimable(bool isDaily)
    {
        if (isDaily)
        {
            dailyCount++;
        }
        else
        {
            achCount++;
        }
        Refresh();
    }

    public void Refresh()
    {
        if (PlayerDataManager.Instance.data ==null)
        {
            return;
        }

        if (dailyCount > 0 || achCount > 0)
        {
            dotMainMenu.alpha = 1;
            dotLevelSelect.alpha = 1;

            if (dailyCount > 0)
            {
                dotDailyTab.alpha = 1;
            }
            else
            {
                dotDailyTab.alpha = 0;
            }

            if (achCount > 0)
            {
                dotAchTab.alpha = 1;
            }
            else
            {
                dotAchTab.alpha = 0;
            }
        }
        else
        {
            dotMainMenu.alpha = 0;
            dotLevelSelect.alpha = 0;
            dotDailyTab.alpha = 0;
            dotAchTab.alpha = 0;
        }
    }

    public void Claimed(bool isDaily)
    {
        if (isDaily)
        {
            dailyCount--;
        }
        else
        {
            achCount--;
        }
        Refresh();
    }
}
