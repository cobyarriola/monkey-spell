using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct TutorialSequence{
    public string dialogue;
    public GameObject highlight;
}

public class TutorialManager : MonoBehaviour
{
    
    public static TutorialManager Instance => FindObjectOfType<TutorialManager>();

    public GameObject BananaClickedPopup;
    public GameObject[] TutorialDialoguePopup;
    public TutorialSequence[] tutorialSequences;
    public GameObject blockTaps;
    public GameObject correctDialogue;
    public GameObject wrongDialogue;
    public GameObject multiplierDialogue;
    public GameObject ReadyPlayDialogue;
    
    private int counter;
    private int bananaTouched;
    private int index;


    public void Awake()
    {
        //InitTutorial();
    }

    public void InitTutorial()
    {
        // GameManager.TutorialStarted -= TutorialDialogue;
        // GameManager.OnBananaClicked -= BananaClicked;
        // SpellZone.OnCorrect -= CorrectDialogue;
        // SpellZone.OnWrong -= WrongDialogue;
        // SpellZone.OnMultiplierUp -= MultiplierDialogue;
        GameManager gameManager = GameManager.Instance;
        SpellZone spellZone = SpellZone.Instance;

        gameManager.TutorialStarted = TutorialDialogue;
        gameManager.OnBananaClicked = BananaClicked;
        spellZone.OnCorrect = CorrectDialogue;
        spellZone.OnWrong = WrongDialogue;
        spellZone.OnMultiplierUp = MultiplierDialogue;

        counter = 0;
        index = 0;
    }

    private void MultiplierDialogue()
    {
        multiplierDialogue.SetActive(true);
    }

    private void CorrectDialogue()
    {
        counter++;
        if(counter <= 3)
        {
            BananaClickedPopup.SetActive(false);
            GameManager.Instance.OnBananaClicked -= BananaClicked;
            correctDialogue.SetActive(true);
        }
        else 
        {
            ReadyPlayDialogue.SetActive(true);       
        }

    }

    private void WrongDialogue()
    {
        BananaClickedPopup.SetActive(false);
        GameManager.Instance.OnBananaClicked -= BananaClicked;
        if(SpellZone.Instance.health.health >= 2) wrongDialogue.SetActive(true);
        counter = 0;
    }

    public void BananaClicked()
    {
        bananaTouched++;
        StartCoroutine(ShowDialogue());      
        
        if (bananaTouched <= 1)
        {
            SpellZone.Instance.tutorialFirstWord = true;
        }

        // SpellZone.Instance.SetTimer(30, Difficulty.Easy);
    }

    IEnumerator ShowDialogue()
    {
        yield return new WaitForSeconds(2);
        blockTaps.SetActive(true);
        
        Next();
    }

    public void Next()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        BananaClickedPopup.SetActive(true);
        BananaClickedPopup.GetComponentInChildren<Text>().text = tutorialSequences[index].dialogue; 
        tutorialSequences[index].highlight.SetActive(true);
    }

    public void NextDialogue()
    {
        if(index < tutorialSequences.Length - 1)
        {
            index++;
            Next();
            tutorialSequences[index-1].highlight.SetActive(false);
        }
        else
        {
            BananaClickedPopup.SetActive(false);
            tutorialSequences[index].highlight.SetActive(false);
            blockTaps.SetActive(false);
        }
    }

    public void SetActiveFalse(GameObject obj)
    {
        SFXMgr.MyInstance.PlaySound("Select");
        obj.SetActive(false);
    }

    public void TutorialDialogue()
    {
        Debug.Log("Tutorial Dialogue");
    }
}

