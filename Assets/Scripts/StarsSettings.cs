using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarsSettings : MonoBehaviour
{
    [System.Serializable]
    public struct GradeStar
    {
        public GradeLevel level;
        public int threeStars;
        public int twoStars;
        public int oneStar;
    }

    [SerializeField]
    private GradeStar[] gradeStars;

    public GradeStar GetGradeStar(int index)
    {
        return gradeStars[index];
    }
}
