using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SaveLoadManager : MonoBehaviour
{
    private static SaveLoadManager instance;
    public static SaveLoadManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SaveLoadManager>();
            }

            return instance;
        }
    }

    [SerializeField]
    private TMP_InputField playerNameInput;

    [SerializeField]
    private MainMenuManager mainMgr;

    private string currentlySelected;

    [SerializeField]
    private Text textShowName;

    [SerializeField]
    private Text textSelectProfile;

    [SerializeField]
    private GradeButtonStars[] gradeButtonStars;

    [SerializeField]
    private GameObject[] menuButtons;

    [SerializeField]
    private Sprite blueButton, greyedButton;

    [SerializeField]
    private string[] achievementsID;

    [SerializeField]
    private int maxPlayers;

    [SerializeField]
    private TextMeshProUGUI errorText;

    private bool hasSelected;

    [SerializeField]
    private GameObject noProfilePrompt;
    public void CreatePlayer()
    {
        if (!File.Exists(Application.persistentDataPath + "/" + playerNameInput.text + ".txt"))
        {
            if (playerNameInput.text.Length > 0)
            {
                if (playerNameInput.text.Length > 13)
                {
                    errorText.text = "Maximum number of characters reached.";
                    return;
                }
                PlayerDataManager.Instance.Create(playerNameInput.text, achievementsID);
                mainMgr.OpenCloseCreationScreen();
                mainMgr.ProfileScreenBackButton();
                noProfilePrompt.SetActive(false);
                ApplyData();
                playerNameInput.text = string.Empty;
            }
            else
            {
                errorText.text = "Please enter a name.";
                //nothing entered
            }
        }
        else
        {
            errorText.text = "Profile name already exists.";
            //profile exists
        }
    }

    public void LoadProfile(string fileName)
    {
        if (File.Exists(Application.persistentDataPath + "/" + fileName + ".txt"))
        {
            noProfilePrompt.SetActive(false);
            PlayerDataManager.Instance.Load(fileName);
            ApplyData();
        }
        else
        {
            NoProfile();
        }
    }

    public void SetCurrentlySelected(string name)
    {
        StopAllCoroutines();
        currentlySelected = name;
        hasSelected = true;
    }

    public bool HasSelected()
    {
        return hasSelected;
    }

    public IEnumerator Deselect()
    {
        yield return new WaitForSeconds(.1f);
        hasSelected = false;
        currentlySelected = string.Empty;
    }

    public void SelectProfile()
    {
        if (currentlySelected != string.Empty)
        {
            LoadProfile(currentlySelected);
            mainMgr.ProfileScreenBackButton();
        }
    }
    private void Awake()
    {
        if (PlayerPrefs.GetString("LastPlayer") != string.Empty)
        {
            LoadProfile(PlayerPrefs.GetString("LastPlayer"));
        }
    }

    public void NoProfile()
    {
        noProfilePrompt.SetActive(true);
        textShowName.text = "No Profile Selected";
        textSelectProfile.text = "Click here to create a profile";
        for (int i = 0; i < menuButtons.Length; i++)
        {
            menuButtons[i].GetComponent<Image>().sprite = greyedButton;
            menuButtons[i].GetComponent<Button>().interactable = false;
        }
    }

    public void ApplyData()
    {
        PlayerPrefs.SetString("LastPlayer", PlayerDataManager.Instance.data.name);
        textShowName.text = "Hello " + PlayerDataManager.Instance.data.name + "!";
        textSelectProfile.text = "If this is not you, press here";
        for (int i = 0; i < menuButtons.Length; i++)
        {
            menuButtons[i].GetComponent<Image>().sprite = blueButton;
            menuButtons[i].GetComponent<Button>().interactable = true;
        }

        for (int i = 0; i < 6; i++)
        {
            gradeButtonStars[i].ChangeStars(PlayerDataManager.Instance.data.GetData(i).stars);
        }
    }
}
