using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DailyMission : MonoBehaviour
{
    private static DailyMission instance;
    public static DailyMission Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<DailyMission>();
            }

            return instance;
        }
    }

    [System.Serializable]
    public struct DailyData
    {
        public string id;
        public string title;
        public int goal;
        public int award;
    }

    [SerializeField]
    private DailyData[] dailyList;

    [SerializeField]
    private Achievement[] dailyContainers;

    private int[] indexArray = new int[3];

    [SerializeField]
    private GameObject missionsPanel;

    [SerializeField]
    private TextMeshProUGUI infoText;

    private void Awake()
    {
        LoadingDaily();
        CheckDate();
    }

    public void CheckDate()
    {
        infoText.text = "";
        infoText.gameObject.SetActive(false);
        infoText.color = Color.white;
        missionsPanel.SetActive(true);

        if (PlayerPrefs.GetString("LastDate") != System.DateTime.Today.ToString())
        {
            PlayerPrefs.SetString("LastDate", System.DateTime.Today.ToString());
            NewDaily();
        }
        else
        {
            indexArray[0] = PlayerPrefs.GetInt("Mission1Index");
            indexArray[1] = PlayerPrefs.GetInt("Mission2Index");
            indexArray[2] = PlayerPrefs.GetInt("Mission3Index");
            UpdateDaily();
        }
    }

    private void NewDaily()
    {
        List<int> list = new List<int>();
        int rand;

        for (int i = 0; i < dailyContainers.Length; i++)
        {
            rand = Random.Range(0, dailyList.Length);

            while (list.Contains(rand))
            {
                rand = Random.Range(0, dailyList.Length);
            }

            list.Add(rand);
        }

        PlayerPrefs.SetInt("Mission1Index", list[0]);
        PlayerPrefs.SetInt("Mission2Index", list[1]);
        PlayerPrefs.SetInt("Mission3Index", list[2]);

        for (int i = 0; i < dailyContainers.Length; i++)
        {
            indexArray[i] = list[i];
        }

        UpdateDaily();
    }

    private void UpdateDaily()
    {
        for (int i = 0; i < dailyContainers.Length; i++)
        {
            dailyContainers[i].InitValues(dailyList[indexArray[i]].goal, dailyList[indexArray[i]].award, dailyList[indexArray[i]].title);
        }
    }

    public void AddDailyToNewPlayer()
    {
        for (int i = 0; i < dailyContainers.Length; i++)
        {
            PlayerDataManager.Instance.data.GetDailies(i).id = dailyList[indexArray[i]].id;
        }
        PlayerDataManager.Instance.Save();
    }

    public void NewlyLoadedPlayer()
    {
        RedNotifier.Instance.ClearDaily();

        if (PlayerDataManager.Instance.data.lastLogin != System.DateTime.Today.ToString())
        {
            PlayerDataManager.Instance.data.lastLogin = System.DateTime.Today.ToString();
            for (int i = 0; i < dailyContainers.Length; i++)
            {
                PlayerDataManager.Instance.data.GetDailies(i).id = dailyList[indexArray[i]].id;
                PlayerDataManager.Instance.data.GetDailies(i).value = 0;
                PlayerDataManager.Instance.data.GetDailies(i).claimed = false;
            }
            PlayerDataManager.Instance.Save();
        }

        for (int i = 0; i < dailyContainers.Length; i++)
        {
            dailyContainers[i].UpdateProgress(PlayerDataManager.Instance.data.GetDailies(i).value, PlayerDataManager.Instance.data.GetDailies(i).claimed, i.ToString());
        }
    }

    public void ProgressDaily()
    {
        RedNotifier.Instance.ClearDaily();

        if (PlayerDataManager.Instance.data == null)
        {
            return;
        }

        for (int i = 0; i < dailyContainers.Length; i++)
        {
            dailyContainers[i].UpdateProgress(PlayerDataManager.Instance.data.GetDailies(i).value, PlayerDataManager.Instance.data.GetDailies(i).claimed, i.ToString());
        }
    }

    public void LoadingDaily()
    {
        infoText.gameObject.SetActive(true);
        infoText.text = "Loading...";
    }
}
