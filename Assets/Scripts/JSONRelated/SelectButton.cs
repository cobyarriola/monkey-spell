using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectButton : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        if (GetComponent<Button>().interactable == true && SaveLoadManager.Instance.HasSelected())
        {
            SaveLoadManager.Instance.SelectProfile();
        }
    }
}
