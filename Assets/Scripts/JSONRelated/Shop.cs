using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;
using TMPro;

public class Shop : MonoBehaviour
{
    [System.Serializable]
    public struct Skin
    {
        public string skinName;
        public int price;
    }

    [SerializeField]
    private Skin[] skins;

    [SerializeField]
    private string[] playerSkins;

    [SerializeField]
    private SkeletonGraphic monkey;

    private int iteration;

    [SerializeField]
    private GameObject nextButton, prevButton;

    [SerializeField]
    private Sprite blueEnabled, greyDisabled;

    private bool isNextDisabled, isPrevDisabled;

    [SerializeField]
    private GameObject confirmButton;

    [SerializeField]
    private TextMeshProUGUI confirmText;

    [SerializeField]
    private Sprite confirmBlue, confirmGrey;

    [SerializeField]
    private TextMeshProUGUI bananas;

    [SerializeField]
    private GameObject bananaIcon;

    private void Awake()
    {
        playerSkins = new string[skins.Length];
        prevButton.GetComponent<Button>().interactable = false;
        prevButton.GetComponent<Image>().sprite = greyDisabled;
        isPrevDisabled = true;
    }

    public void UpdateSkinsData()
    {
        for (int i = 0; i < playerSkins.Length; i++)
        {
            playerSkins[i] = string.Empty;
        }

        PlayerDataManager dataManager = PlayerDataManager.Instance;
        for (int i = 0; i < playerSkins.Length; i++)
        {
            playerSkins[i] = dataManager.data.mySkins[i];
        }
        bananas.text = dataManager.data.bananas.ToString();

        CheckSkin();
        bananas.text = PlayerDataManager.Instance.data.bananas.ToString();
    }

    private void CheckSkin()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        monkey.Skeleton.SetSkin(skins[iteration].skinName);
        confirmText.alignment = TextAlignmentOptions.Center;
        bananaIcon.SetActive(false);

        if (dataManager.data.equippedSkin == skins[iteration].skinName)
        {
            confirmButton.GetComponent<Button>().interactable = false;
            confirmButton.GetComponent<Image>().sprite = confirmGrey;
            confirmText.text = "Using";
        }
        else if (playerSkins[iteration] == skins[iteration].skinName)
        {
            confirmButton.GetComponent<Button>().interactable = true;
            confirmButton.GetComponent<Image>().sprite = confirmBlue;
            confirmText.text = "Use";
        }
        else
        {
            if (skins.Length-1 == iteration)
            {
                confirmText.text = "Carrot?";
                confirmButton.GetComponent<Button>().interactable = false;
                confirmButton.GetComponent<Image>().sprite = confirmGrey;
                return;
            }

            confirmText.text = "      " + skins[iteration].price.ToString();
            bananaIcon.SetActive(true);
            //confirmText.alignment = TextAlignmentOptions.Right;


            if (dataManager.data.bananas >= skins[iteration].price)
            {
                confirmButton.GetComponent<Button>().interactable = true;
                confirmButton.GetComponent<Image>().sprite = confirmBlue;             
            }
            else
            {
                confirmButton.GetComponent<Button>().interactable = false;
                confirmButton.GetComponent<Image>().sprite = confirmGrey;
            }
        }
    }

    public void Confirm()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        if (playerSkins[iteration] == skins[iteration].skinName)
        {
            dataManager.data.equippedSkin = skins[iteration].skinName;
            CheckSkin();
        }
        else if (dataManager.data.bananas >= skins[iteration].price)
        {
            dataManager.data.bananas -= skins[iteration].price;
            dataManager.data.mySkins[iteration] = skins[iteration].skinName;
            SFXMgr.MyInstance.PlaySound("CashRegister");
            UpdateSkinsData();
        }
        dataManager.Save();
    }

    public void Next()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        iteration++;
        CheckSkin();

        if (iteration >= skins.Length-1)
        {
            nextButton.GetComponent<Button>().interactable = false;
            nextButton.GetComponent<Image>().sprite = greyDisabled;
            isNextDisabled = true;
        }

        if (isPrevDisabled)
        {
            prevButton.GetComponent<Button>().interactable = true;
            prevButton.GetComponent<Image>().sprite = blueEnabled;
            isPrevDisabled = false;
        }
    }

    public void Previous()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        iteration--;
        CheckSkin();

        if (iteration <= 0)
        {
            prevButton.GetComponent<Button>().interactable = false;
            prevButton.GetComponent<Image>().sprite = greyDisabled;
            isPrevDisabled = true;
        }

        if (isNextDisabled)
        {
            nextButton.GetComponent<Button>().interactable = true;
            nextButton.GetComponent<Image>().sprite = blueEnabled;
            isNextDisabled = false;
        }
    }
}
