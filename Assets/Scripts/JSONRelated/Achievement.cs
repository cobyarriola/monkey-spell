using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Achievement : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI titleText;
    [SerializeField]
    private TextMeshProUGUI awardText;

    [SerializeField]
    private Image progressBar;
    [SerializeField]
    private TextMeshProUGUI progressText;

    [SerializeField]
    private Button myButton;

    [SerializeField]
    private TextMeshProUGUI bananasText;

    [SerializeField]
    private GameObject BananaExplosionPrefab;

    [SerializeField]
    private GameObject Canvas;

    [SerializeField]
    private bool isDaily;

    private int goal;

    private int award;

    private string id;

    void Start()
    {
        //BananaExplosionPrefab = Resources.Load<GameObject>("BananaExplode");
        
    }

    public void InitValues(int newGoal, int newAward, string title)
    {
        this.goal = newGoal;
        this.award = newAward;
        awardText.text = award.ToString();
        titleText.text = title;
        titleText.fontSize = 22;
        progressText.fontSize = 22;
    }

    public void UpdateProgress(int progress, bool claimed, string id)
    {
        float val = (float)progress / (float)goal;
        progressBar.fillAmount = val;

        this.id = id;

        myButton.interactable = false;

        if (claimed)
        {
            progressText.text = "COMPLETED";
        }
        else
        {
            progressText.text = progress.ToString() + "/" + goal.ToString();

            if (progress >= goal)
            {
                myButton.interactable = true;
            }
        }

        //Notification
        if (progress >= goal && !claimed)
        {
            RedNotifier.Instance.HasClaimable(isDaily);
        }
    }

    public void ClaimReward()
    {
        PlayerDataManager.Instance.data.bananas += award;

        GameObject banana = Instantiate(BananaExplosionPrefab, myButton.transform.position, Quaternion.identity, Canvas.transform);

        SFXMgr.MyInstance.PlaySound("ClaimMoney");

        if (PlayerDataManager.Instance.data.GetAchieveWrite(id).claimed2 == true)
        {
            PlayerDataManager.Instance.data.GetAchieveWrite(id).claimed3 = true;
            myButton.interactable = false;
            progressText.text = "COMPLETED";
        }
        else if (PlayerDataManager.Instance.data.GetAchieveWrite(id).claimed1 == true)
        {
            PlayerDataManager.Instance.data.GetAchieveWrite(id).claimed2 = true;
        }
        else
        {
            PlayerDataManager.Instance.data.GetAchieveWrite(id).claimed1 = true;
        }

        bananasText.text = PlayerDataManager.Instance.data.bananas.ToString();
        PlayerDataManager.Instance.Save();

        AchieveData.Instance.UpdateSpecific(id);
        RedNotifier.Instance.Claimed(isDaily);
    }
    public void ClaimDailyMission()
    {
        GameObject banana = Instantiate(BananaExplosionPrefab, myButton.transform.position, Quaternion.identity);
        SFXMgr.MyInstance.PlaySound("ClaimMoney");
        PlayerDataManager.Instance.data.bananas += award;
        PlayerDataManager.Instance.data.GetDailies(int.Parse(id)).claimed = true;
        progressText.text = "COMPLETED";
        bananasText.text = PlayerDataManager.Instance.data.bananas.ToString();
        myButton.interactable = false;
        PlayerDataManager.Instance.Save();
        RedNotifier.Instance.Claimed(isDaily);
    }
}
