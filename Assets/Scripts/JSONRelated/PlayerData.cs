using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    public string name = "";

    public int bananas = 0;

    public string lastLogin = "";

    public bool finishedTutorial = false;

    public string equippedSkin = "";

    public string[] mySkins = new string[8];

    public GradeData[] myGradeData = new GradeData[6];

    public AchieveData[] myAchievements = new AchieveData[8];

    public DailyData[] myDailies = new DailyData[3];

    [System.Serializable]
    public struct GradeData
    {
        public int stars;
        public int streak;
        public int highwords;
        public int highscore;
    }

    [System.Serializable]
    public struct AchieveData
    {
        public string id;
        public int value;
        public bool claimed1;
        public bool claimed2;
        public bool claimed3;
    }

    [System.Serializable]
    public struct DailyData
    {
        public string id;
        public int value;
        public bool claimed;
    }

    public ref GradeData GetData(int index)
    {
        return ref myGradeData[index];
    }

    public ref DailyData GetDailies(int index)
    {
        return ref myDailies[index];
    }
    public int DailyLength()
    {
        return myDailies.Length;
    }

    public ref AchieveData GetAchieveWrite(string id)
    {
        for (int i = 0; i < myAchievements.Length; i++)
        {
            if (id == myAchievements[i].id)
            {
                return ref myAchievements[i];
            }
        }

        return ref myAchievements[99];
    }
    public AchieveData GetAchieveRead(string id)
    {
        for (int i = 0; i < myAchievements.Length; i++)
        {
            if (id == myAchievements[i].id)
            {
                return myAchievements[i];
            }
        }

        return myAchievements[99];
    }

    public void WriteAchievementID(int index, string id)
    {
        myAchievements[index].id = id;
    }
}
