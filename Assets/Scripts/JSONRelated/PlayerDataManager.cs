using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlayerDataManager : MonoBehaviour
{
    private static PlayerDataManager instance;
    public static PlayerDataManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<PlayerDataManager>();
            }

            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
        else
        {
            Destroy(transform.gameObject);
        }
    }

    public PlayerData data;

    private string file;

    public void Create(string playerName, string[] achievements)
    {
        file = playerName + ".txt";
        data = new PlayerData();
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, data);
        data.name = playerName;
        for (int i = 0; i < achievements.Length; i++)
        {
            data.WriteAchievementID(i, achievements[i]);
        }
        data.mySkins[0] = "RegMonke";
        data.equippedSkin = data.mySkins[0];

        DailyMission.Instance.AddDailyToNewPlayer();
        DailyMission.Instance.NewlyLoadedPlayer();
        AchieveData.Instance.NewValues();
        AchieveData.Instance.UpdateValues();
        RedNotifier.Instance.Refresh();
        Save();
    }

    public void Save()
    {
        string json = JsonUtility.ToJson(data);
        WriteToFile(file, json);
    }

    public void Load(string name)
    {
        file = name + ".txt";
        data = new PlayerData();
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, data);
        DailyMission.Instance.NewlyLoadedPlayer();
        AchieveData.Instance.NewValues();
        AchieveData.Instance.UpdateValues();
        RedNotifier.Instance.Refresh();
    }

    private void WriteToFile(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);

        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(json);
        }
    }

    private string ReadFromFile(string fileName)
    {
        string path = GetFilePath(fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
        {
            Debug.LogWarning("File not found!");
            return "";
        }
    }

    private string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }


    // Update is called once per frame
    void OnEnable()
    {
        DirectoryInfo dir = new DirectoryInfo(GetFilePath(""));
        FileInfo[] info = dir.GetFiles("*.*");
        foreach (FileInfo f in info)
        {

        }
    }
}
