using UnityEngine;
using UnityEngine.UI;

public class Ribbon : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup dailyTab, awardTab;

    [SerializeField]
    private Sprite selected, notSelected;

    [SerializeField]
    private GameObject dailyButton, awardButton;

    public void SelectTab()
    {
        SFXMgr.MyInstance.PlaySound("Select");
        awardTab.interactable = awardTab.interactable ? false : true;
        awardTab.blocksRaycasts = awardTab.interactable ? true : false;
        awardTab.alpha = awardTab.interactable ? 1 : 0;
        awardButton.GetComponent<Button>().interactable = awardTab.interactable ? false : true;
        awardButton.GetComponent<Image>().sprite = awardTab.interactable? selected : notSelected;

        dailyTab.interactable = dailyTab.interactable ? false : true;
        dailyTab.blocksRaycasts = dailyTab.interactable ? true : false;
        dailyTab.alpha = dailyTab.interactable ? 1 : 0;
        dailyButton.GetComponent<Button>().interactable = dailyTab.interactable ? false : true;
        dailyButton.GetComponent<Image>().sprite = dailyTab.interactable ? selected : notSelected;
    }
}
