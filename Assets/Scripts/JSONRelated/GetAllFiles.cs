using UnityEngine;
using System.IO;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;

public class GetAllFiles : MonoBehaviour
{
    public PlayerData data;

    [SerializeField]
    private GameObject profilePrefab;

    [SerializeField]
    private Scrollbar scrollbar;

    [SerializeField]
    private List<ProfileButton> profileContainers;

    [SerializeField]
    private GameObject SelectButton;
    [SerializeField]
    private GameObject RibbonButton;
    [SerializeField]
    private GameObject ShopButton;

    [SerializeField]
    private Sprite blueButton, greyedButton;
    // Update is called once per frame
    void OnEnable()
    {
        DirectoryInfo dir = new DirectoryInfo(GetFilePath(""));
        FileInfo[] info = dir.GetFiles("*.*");
        for (int i = 0; i < info.Length; i++)
        {
            GameObject newProfile = Instantiate(profilePrefab, transform);
            profileContainers.Add(newProfile.GetComponent<ProfileButton>());
            data = new PlayerData();
            string json = ReadFromFile(info[i].Name);
            JsonUtility.FromJsonOverwrite(json, data);
            profileContainers[i].myText.text = data.name;
        }

        SelectButton.GetComponent<Button>().interactable = info.Length > 0 ? true : false;
        SelectButton.GetComponent<Image>().sprite = info.Length > 0 ? blueButton : greyedButton;
        scrollbar.value = 1;
    }

    private void OnDisable()
    {
        for (int i = 0; i < profileContainers.Count; i++)
        {
            Destroy(profileContainers[i].gameObject);
        }
        profileContainers.Clear();
    }

    private string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }
    private string ReadFromFile(string fileName)
    {
        string path = GetFilePath(fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
        {
            Debug.LogWarning("File not found!");
            return "";
        }
    }
}
