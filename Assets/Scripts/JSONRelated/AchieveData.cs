using UnityEngine;
using TMPro;

public class AchieveData : MonoBehaviour
{
    private static AchieveData instance;
    public static AchieveData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AchieveData>();
            }

            return instance;
        }
    }

    [System.Serializable]
    public struct AchievementData
    {
        public string id;
        public string title;
        public int goal1;
        public int goal2;
        public int goal3;
        public int award1;
        public int award2;
        public int award3;
    }

    [SerializeField]
    private TextMeshProUGUI bananas;

    [SerializeField]
    private AchievementData[] achievementDatas;

    private Achievement[] achievements;

    private void Awake()
    {
        achievements = GetComponentsInChildren<Achievement>();
    }

    public void UpdateValues()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        if (dataManager.data != null)
        {
            bananas.text = dataManager.data.bananas.ToString();
        }

        RedNotifier.Instance.ClearAch();

        for (int i = 0; i < achievementDatas.Length; i++)
        { 
            if (dataManager.data == null)
            {
                achievements[i].UpdateProgress(0, false, "");
                continue;
            }

            achievements[i].UpdateProgress(dataManager.data.GetAchieveRead(achievementDatas[i].id).value, dataManager.data.GetAchieveRead(achievementDatas[i].id).claimed3, achievementDatas[i].id);
        }
    }

    public void NewValues()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        RedNotifier.Instance.ClearAch();

        if (dataManager.data == null)
        {
            return;
        }

        for (int i = 0; i < achievementDatas.Length; i++)
        {
            if (dataManager.data.GetAchieveRead(achievementDatas[i].id).claimed2)
            {
                achievements[i].InitValues(achievementDatas[i].goal3, achievementDatas[i].award3, achievementDatas[i].title);
            }
            else if (dataManager.data.GetAchieveRead(achievementDatas[i].id).claimed1)
            {
                achievements[i].InitValues(achievementDatas[i].goal2, achievementDatas[i].award2, achievementDatas[i].title);
            }
            else
            {
                achievements[i].InitValues(achievementDatas[i].goal1, achievementDatas[i].award1, achievementDatas[i].title);
            }

            achievements[i].UpdateProgress(dataManager.data.GetAchieveRead(achievementDatas[i].id).value, dataManager.data.GetAchieveRead(achievementDatas[i].id).claimed3, achievementDatas[i].id);
        }
    }

    public void UpdateSpecific(string id)
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;

        for (int i = 0; i < achievementDatas.Length; i++)
        {
            if (achievementDatas[i].id == id)
            {
                if (dataManager.data.GetAchieveRead(id).claimed2)
                {
                    achievements[i].InitValues(achievementDatas[i].goal3, achievementDatas[i].award3, achievementDatas[i].title);
                }
                else if (dataManager.data.GetAchieveRead(id).claimed1)
                {
                    achievements[i].InitValues(achievementDatas[i].goal2, achievementDatas[i].award2, achievementDatas[i].title);
                }
                else
                {
                    achievements[i].InitValues(achievementDatas[i].goal1, achievementDatas[i].award1, achievementDatas[i].title);
                }

                achievements[i].UpdateProgress(dataManager.data.GetAchieveRead(id).value, dataManager.data.GetAchieveRead(id).claimed3, id);
                break;
            }
        }
    }
}
