using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;

public class ProfileButton : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public TextMeshProUGUI myText;

    [SerializeField]
    private Image myBackground;

    private void Awake()
    {
        myText = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        StartCoroutine(SaveLoadManager.Instance.Deselect()); 
        myBackground.color = new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, .5f);
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (myText.text != string.Empty)
        {
            SaveLoadManager.Instance.SetCurrentlySelected(myText.text);
            myBackground.color = new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, .3f);
        }
    }
}
