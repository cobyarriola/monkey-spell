using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradeButtonStars : MonoBehaviour
{
    [SerializeField]
    private Sprite filledStar, blankStar;

    [SerializeField]
    private Image[] stars;

    public void ChangeStars(int val)
    {
        foreach (var image in stars)
        {
            image.sprite = blankStar;
        }

        for (int i = 0; i < val; i++)
        {
            stars[i].sprite = filledStar;
        }
    }
}
