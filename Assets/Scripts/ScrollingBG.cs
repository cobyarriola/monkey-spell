using System.Collections;
using UnityEngine;

public class ScrollingBG : MonoBehaviour
{
    private float moveSpeed;

    [SerializeField]
    private float range;

    private float moveTime = 1;

    public IEnumerator MoveDownwards()
    {
        float tmp = moveTime;
        while (moveTime > 0)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + (Vector3.down * range), moveSpeed * Time.deltaTime);
            moveTime -= 1 * Time.deltaTime;
            yield return null;
        }
        moveTime = 1;
    }
    public IEnumerator MoveNew(float newTime)
    {
        float tmp = newTime;
        while (tmp > 0)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + (Vector3.down * range), moveSpeed * Time.deltaTime);
            tmp -= 1 * Time.deltaTime;
            yield return null;
        }
    }
    public void ChangeRange(float newRange)
    {
        moveSpeed = newRange;
    }
    public float myRange()
    {
        return moveSpeed;
    }

    public float myTime()
    {
        return moveTime;
    }
}
