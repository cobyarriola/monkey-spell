using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour
{
    private GradeLevel gradeLevel;

    private int oneStar, twoStars, threeStars;

    public bool backToLevelSelect = false;

    public GameObject[] db;
    private static DataManager instance;
    public static DataManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<DataManager>();
            }

            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        else
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            Destroy(transform.gameObject);
        }
    }
    public void SetDataBase(GameObject database, int i)
    {
        db[i] = database;
    }

    public void Destroy()
    {
        for(int i = 0; i < db.Length; i++)
        {
            if(db[i] != null)
            {
                Destroy(db[i]);
            }
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.SetGradeLevel(gradeLevel);
            GameManager.Instance.SetStarValues(oneStar, twoStars, threeStars);
        }
    }

    public void ChangeStarValues(int one, int two, int three)
    {
        oneStar = one;
        twoStars = two;
        threeStars = three;
    }

    public void ChangeGradeLevel(GradeLevel grade)
    {
        gradeLevel = grade;
    }

    public bool isLevelSelect()
    {
        return backToLevelSelect;
    }
}
