using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXMgr : MonoBehaviour
{
    private static SFXMgr instance;
    public static SFXMgr MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SFXMgr>();
            }

            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
        else
        {
            Destroy(transform.gameObject);
        }
    }

    [SerializeField]
    private AudioSource[] SFX
    {
        get
        {
            return GetComponentsInChildren<AudioSource>();
        }
    }

    public void PlaySound(string name)
    {
        foreach (AudioSource sound in SFX)
        {
            if (sound.name.Contains(name))
            {
                sound.Play();
            }
        }
    }

    public void SetVolume(string name)
    {
        foreach (AudioSource item in SFX)
        {
            if(item.name.Contains(name))
            {
                item.volume = 1;
            }
        }
    }

    public void StopSound(string name)
    {
        foreach (AudioSource item in SFX)
        {
            if(item.name.Contains(name))
            {
                item.Stop();
            }
        }
    }

    public bool isPlaying(string name)
    {
        foreach (AudioSource item in SFX)
        {
            if(item.name.Contains(name))
            {
                if(item.isPlaying)
                {
                    return true;
                }              
            }
        }
        //Debug.Log("is not playing return false");
        return false;
    }

    public void StopAllSounds(string name)
    {
        foreach (AudioSource item in SFX)
        {
            if(item.name != name)
            {
                item.Stop();
            }     
        }
    }

    public void PauseSounds(string name)
    {
        foreach (AudioSource item in SFX)
        {
            if(item.name != name)
            {
                item.Pause();   
            }
        }
    }

    public void Unpause()
    {
        foreach (AudioSource item in SFX)
        {
            item.UnPause();
        }
    }

    public void AdjustVolume(string name, float targetVolume, float duration)
    {
        foreach (AudioSource item in SFX)
        {
            if(item.name.Contains(name))
            {
                if(isPlaying(item.name))
                {
            
                    float currentTime = 0;
                    float start = item.volume;

                    while(currentTime < duration)
                    {
                        currentTime += Time.deltaTime;
                        item.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                    }

                    if(targetVolume == 0)
                    {
                        item.Stop(); return;
                        // should be fading out
                    }
                }
            }

        }
    }
}
