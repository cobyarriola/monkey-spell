using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{

    public Text TextDisplay;
    public string[] Sentences;
    private int index;
    public float TypingSpeed;
    private bool tickStatus = false;

    public GameObject DialoguePopup;
    public GameObject ContinueButton;
    public GameObject Canvas;

    // Start is called before the first frame update
    void Start()
    {
        DialoguePopup.SetActive(true);
        StartCoroutine(Type());
    }

    void Update()
    {
        if(TextDisplay.text == Sentences[index])
        {
            ContinueButton.SetActive(true);
            TypeSound(false);
        }
    }

    IEnumerator Type()
    {
        foreach(char letter in Sentences[index].ToCharArray())
        {
            TextDisplay.text += letter;
            TypeSound(true);
            yield return new WaitForSeconds(TypingSpeed);
        }
    }

    public void TypeSound(bool value)
    {
        if(value == true && tickStatus == false)
            {
                tickStatus = true;
                SFXMgr.MyInstance.PlaySound("KeyboardType");
            } 
            else if (value == false)
            {
                tickStatus = false;
                SFXMgr.MyInstance.StopSound("KeyboardType");
            }  
    }

    public void NextSentence()
    {
        ContinueButton.SetActive(false);
        SFXMgr.MyInstance.PlaySound("Select");
        if(index < Sentences.Length - 1)
        {
            index++;
            TextDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            TextDisplay.text = "";
            ContinueButton.SetActive(false);
            DialoguePopup.SetActive(false);
            Canvas.SetActive(false);
        }
    }
}
