using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTutorial : MonoBehaviour
{

    public GameObject tutorial;

    void Start()
    {
        GameObject tutorialObj = Instantiate(tutorial, transform.position, Quaternion.identity);
        tutorialObj.GetComponentInChildren<TutorialManager>().InitTutorial();
    }

    void OnSceneLoaded()
    {
        
    }

}
